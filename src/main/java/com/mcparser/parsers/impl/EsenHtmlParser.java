package com.mcparser.parsers.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import com.mcparser.analysers.impl.SubjectAnalyser;
import com.mcparser.analysers.impl.TopicAnalyser;
import com.mcparser.replacers.ImageReplacer;
import com.mcparser.replacers.impl.DummyImageReplacer;
import com.mcparser.replacers.impl.MongoImageReplacer;
import com.mcparser.structures.Image;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeVisitor;

import com.mcparser.structures.CorrectResponse;
import com.mcparser.structures.Question;
import com.mcparser.structures.Test;
import com.mcparser.parsers.AbstractFileParser;

/**
 * A file replacer for ESEN publisher HTML files (which are obtained through
 * Indesign export).
 */
public class EsenHtmlParser extends AbstractFileParser {

  public EsenHtmlParser(ImageReplacer imageReplacer) {
    super(imageReplacer);
  }

  // CAUTION: Code in progress.
  // Parses only the first test.
  // Parses only the question text.
  public List<Test> parseFile(File file) throws IOException {

    // Create test array and read the page with jsoup
    List<Test> tests = new ArrayList<Test>();
    Document doc = Jsoup.parse(file, "UTF-8", "");
    Element body = doc.body();

    // For now just read the first test.
    Elements testElements = body
        .getElementsByClass("_idGenObjectStyleOverride-1");
    Element test1 = testElements.get(0);

    StringBuffer questionText = new StringBuffer();
    StringBuffer aChoice = new StringBuffer();
    StringBuffer bChoice = new StringBuffer();
    StringBuffer cChoice = new StringBuffer();
    StringBuffer dChoice = new StringBuffer();
    StringBuffer eChoice = new StringBuffer();

    CorrectResponse correctResponse = CorrectResponse.A;

    // TODO(haliterdogan): Change the interface. We want to keep the original
    // TODO(haliterdogan): questions (so that we can spit out html files for
    // inspection) as well as
    // TODO(haliterdogan): image replaced questions.
    List<Question> originalTestQuestions = new ArrayList<Question>();
    List<Image> testImages = new ArrayList<Image>();
    List<Node> nodeList = new LinkedList<Node>();
    nodeList.add(test1);

    int questionNum = 1;
    StringBuffer currentTextBuffer = questionText;

    // Leaf-based depth-first search like a king!!
    while (!nodeList.isEmpty()) {
      Node node = nodeList.remove(0);
      if (node.childNodeSize() > 0) {
        System.out.println("Non-leaf: " + node.toString());

        // TODO(haliterdogan): Handle last question properly.
        if (isQuestionNode(node, questionNum + 1)
            && currentTextBuffer == eChoice) {
          System.out.println("Creating new question");
          // If this is non-leaf question node and the current text pointer
          // is pointing to the e-choice, then it's time to construct the
          // question. CAUTION: We are comparing the objects here, not the
          // contents.
          originalTestQuestions.add(new Question(questionText.toString(),
              aChoice.toString(), bChoice.toString(), cChoice.toString(),
              dChoice.toString(), eChoice.toString(), correctResponse));
          questionText = new StringBuffer();
          aChoice = new StringBuffer();
          bChoice = new StringBuffer();
          cChoice = new StringBuffer();
          dChoice = new StringBuffer();
          eChoice = new StringBuffer();
          currentTextBuffer = questionText;
          ++questionNum;

        }

        // Push to the front starting from the last to do proper DFS.
        for (int i = node.childNodeSize() - 1; i >= 0; --i) {
          nodeList.add(0, node.childNode(i));
        }
        continue;

      }
      if (node.toString().trim().equals("")) {
        continue;
      }
      System.out.println("Node: " + node.toString());
      // TODO(haliterdogan): This is ugly.
      if (isTextNode(node)) {
        System.out.println("Text Node");

        String rawText = node.toString();
        for (int i = 0; i < rawText.length(); ++i) {
          if (rawText.charAt(i) == 'A' && i < rawText.length() - 1
              && rawText.charAt(i + 1) == ')') {
            currentTextBuffer = aChoice;
            ++i;
            continue;
          }
          if (rawText.charAt(i) == 'B' && i < rawText.length() - 1
              && rawText.charAt(i + 1) == ')') {
            currentTextBuffer = bChoice;
            ++i;
            continue;
          }
          if (rawText.charAt(i) == 'C' && i < rawText.length() - 1
              && rawText.charAt(i + 1) == ')') {
            currentTextBuffer = cChoice;
            ++i;
            continue;
          }
          if (rawText.charAt(i) == 'D' && i < rawText.length() - 1
              && rawText.charAt(i + 1) == ')') {
            currentTextBuffer = dChoice;
            ++i;
            continue;
          }
          if (rawText.charAt(i) == 'E' && i < rawText.length() - 1
              && rawText.charAt(i + 1) == ')') {
            currentTextBuffer = eChoice;
            ++i;
            continue;
          }
          currentTextBuffer.append(rawText.charAt(i));
        }
      }

      if (isImageNode(node)) {
        currentTextBuffer.append(imageReplacer.ReplaceImages((Element) node, testImages).toString());
      }
    }

    tests.add(new Test(originalTestQuestions, testImages, "Esen Matematik 1"));

    return tests;
  }

  private boolean isQuestionNode(Node node, int questionNum) {
    System.out.println("IsQuestion: " + node.toString().contains(">" + Integer.toString(questionNum) + ".<"));
    return node.toString().contains(">" + Integer.toString(questionNum) + ".<");
  }

  private boolean isTextNode(Node node) {
    return node.hasAttr("text");
  }

  private boolean isImageNode(Node node) {
    // TODO(haliterdogan): We should change img tag instead of src attr.
    return node.hasAttr("src");
  }

  private boolean isChoiceNode(Node node, String choice) {
    return node.toString().contains(choice + ")");
  }

  public static void main(String args[]) throws IOException {

    String testFile = "src/main/resources/html-samples/koklu-ifadeler.html";
    EsenHtmlParser e = new EsenHtmlParser(new DummyImageReplacer());
    List<Test> tests = e.parseFile(new File(testFile));

    System.out.println("-------- PRINTING TESTS -------");
    for (Test test : tests) {
      System.out.println(test.toString());
    }

  }
}
