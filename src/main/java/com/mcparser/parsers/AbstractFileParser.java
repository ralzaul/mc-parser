package com.mcparser.parsers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.mcparser.structures.Test;
import com.mcparser.replacers.ImageReplacer;

/**
 * An abstract class to parse multiple choice question files.
 */
public abstract class AbstractFileParser {
    // The files may contain images pointing to local files. @imageReplacer
    // will be used to replace images.
    // TODO(haliterdogan): Should this be specific to htmlparsers package?
    // TODO(cako): sanmam baskan - bizim encoding stratejimiz ile alakali file formatindan ziyade
    protected ImageReplacer imageReplacer;

    public AbstractFileParser(ImageReplacer imageReplacer) {
        this.imageReplacer = imageReplacer;
    }

    /**
     * Parses the given file and returns the list of tests in this file.
     *
     * @param file A file object.
     * @return List of tests.
     * @throws IOException
     */
    public abstract List<Test> parseFile(File file) throws IOException;
}
