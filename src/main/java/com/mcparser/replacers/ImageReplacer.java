package com.mcparser.replacers;

import com.mcparser.structures.Image;
import com.mcparser.structures.Question;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import java.util.List;

/**
 * Image replacer replaces an HTML node and replaces the image sources which are
 * pointing to local files. It pushes the image to the web storage (e.g., DB)
 * and manipulates the given node accordingly.
 */
public interface ImageReplacer {
    public Node ReplaceImages(Element element, List<Image> imageCollection);
}
