package com.mcparser.replacers.impl;

import com.mcparser.clients.DatabaseClient;
import com.mcparser.replacers.ImageReplacer;
import com.mcparser.structures.Image;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Parser;

import java.util.List;

/**
 * MangoDB based image replacer. It takes the HTML node, parses the image
 * sources, inserts the images to the DB, and manipulate the HTML node
 * accordingly.
 * <p>
 */
public class MongoImageReplacer implements ImageReplacer {

    private DatabaseClient databaseClient;

    public MongoImageReplacer(DatabaseClient databaseClient) {
        this.databaseClient = databaseClient;
    }

    public Node ReplaceImages(Element element, List<Image> imageCollection) {

        //Get all elements with img tag ,
        String src = element.attr("src");
        String fileName = src.substring(src.lastIndexOf("/") + 1);
        String fileDirectory = "resources/html-samples/" + src;
        imageCollection.add(new Image(fileName, fileDirectory));
        return Jsoup.parse("getImage(" + fileName + ")", "UTF-8" , Parser.xmlParser());
    }
}
