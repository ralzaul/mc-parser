package com.mcparser.analysers.constants;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

// should be a singleton - after spring integration
public class SubjectTokens {

    public enum SUBJECT {
        MATEMATIK, GEOMETRI, FIZIK, KIMYA, BIYOLOJI, TURKCE, TARIH, COGRAFYA, FELSEFE;
    }

    private ListMultimap<SUBJECT, String> subjectTokenList;

    public SubjectTokens(){
        subjectTokenList = ArrayListMultimap.create();
        this.setMatematikTokens();
        this.setFizikTokens();
        this.setGeometriTokens();
        this.setKimyaTokens();
        this.setTurkceTokens();
        this.setBiyolojiTokens();
        this.setCografyaTokens();
        this.setTarihTokens();
        this.setFelsefeTokens();
    }

    private void setMatematikTokens() {
        subjectTokenList.put(SUBJECT.MATEMATIK, "islem");
        subjectTokenList.put(SUBJECT.MATEMATIK, "toplama");
    }

    private void setFizikTokens() {
        subjectTokenList.put(SUBJECT.FIZIK, "kuvvet");
        subjectTokenList.put(SUBJECT.FIZIK, "vektor");
    }

    private void setGeometriTokens() {
        subjectTokenList.put(SUBJECT.GEOMETRI, "ucgen");
        subjectTokenList.put(SUBJECT.GEOMETRI, "daire");
        subjectTokenList.put(SUBJECT.GEOMETRI, "aci");
    }

    private void setKimyaTokens() {

        subjectTokenList.put(SUBJECT.KIMYA, "molekul");
        subjectTokenList.put(SUBJECT.KIMYA, "mol");
    }

    private void setTurkceTokens() {
        subjectTokenList.put(SUBJECT.TURKCE, "metin");
        subjectTokenList.put(SUBJECT.TURKCE, "bozuklugu");
    }

    private void setBiyolojiTokens() {
        subjectTokenList.put(SUBJECT.BIYOLOJI, "mitakondri");
        subjectTokenList.put(SUBJECT.BIYOLOJI, "hucre");
    }

    private void setCografyaTokens() {
        subjectTokenList.put(SUBJECT.COGRAFYA, "enlem");
        subjectTokenList.put(SUBJECT.COGRAFYA, "boylam");
    }

    private void setTarihTokens() {
        subjectTokenList.put(SUBJECT.TARIH, "osmanli");
        subjectTokenList.put(SUBJECT.TARIH, "devlet");
    }

    private void setFelsefeTokens() {
        subjectTokenList.put(SUBJECT.FELSEFE, "felsefe");
    }






}
