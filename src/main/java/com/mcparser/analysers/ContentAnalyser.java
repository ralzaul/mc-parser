package com.mcparser.analysers;

import com.mcparser.structures.Question;
import com.mcparser.structures.Test;

import java.util.List;

/**
 * Created by senelc on 12/05/2015.
 */
public interface ContentAnalyser {

    /**
     * Analyse the content of the question and deduce on it.
     *
     * @param question A Question object.
     * @return name of the content that is decided after analyze
     */
    public List<String> analyseQuestion(Question question);

    /**
     * Analyse the content of the test and deduce on it.
     *
     * @param test A test object.
     * @return name of the content that is decided after analyze
     */
    public List<String> analyseTest(Test test);


    /**
     * Extracts the id of the content from the topic name
     *
     * @param name A content name.
     * @return topic object with Id an Name
     */
    public String extractIdFromName(String name);
}
