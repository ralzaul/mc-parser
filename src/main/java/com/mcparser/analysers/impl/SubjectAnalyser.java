package com.mcparser.analysers.impl;

import com.mcparser.analysers.ContentAnalyser;
import com.mcparser.analysers.constants.SubjectTokens;
import com.mcparser.structures.Question;
import com.mcparser.structures.Subject;
import com.mcparser.structures.Test;

import java.util.ArrayList;
import java.util.List;

public class SubjectAnalyser implements ContentAnalyser{

    SubjectTokens subjectTokens = new SubjectTokens();

    public List<Subject> extract(Object objectToAnalyze){

        List <String> subjectNameList = new ArrayList<String>();
        if(objectToAnalyze instanceof Question){
            subjectNameList = analyseQuestion((Question)objectToAnalyze);
        } else if (objectToAnalyze instanceof Test){
            subjectNameList = analyseTest((Test) objectToAnalyze);
        } else {
            System.out.println("Object type is not known");
        }

        List <Subject> subjectList = new ArrayList<Subject>();
        for(String subjectName : subjectNameList){
            subjectList.add(new Subject(subjectName, extractIdFromName(subjectName)));
        }

        return subjectList;
    }

    /**
     * Analyse the subject of the question and deduce on it.
     *
     * @param question A Question object.
     * @return name of the subject that is decided after analyze
     */
    public List<String> analyseQuestion(Question question) {
        List<String> subjectList = new ArrayList<String>();
        subjectList.add("Matematik");
        return subjectList;
    }

    /**
     * Analyse the subject of the test and deduce on it.
     *
     * @param test A test object.
     * @return name of the subject that is decided after analyze
     */
    public List<String> analyseTest(Test test) {
        List<String> subjectList = new ArrayList<String>();
        subjectList.add("Matematik");
        return subjectList;
    }

    /**
     * Extracts the id of the subject from the subject name from database
     * TODO actually it should be retrieved at the beginning of application for all topics
     * TODO and then we should always use a hashmap to retrieve it by name not database.
     *
     * @param name A subject name.
     * @return subject object with Id an Name
     */
    public String extractIdFromName(String name){
        String subjectId = "-1";
        return subjectId;
    }
}
