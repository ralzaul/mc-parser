package com.mcparser.analysers.impl;

import com.mcparser.analysers.ContentAnalyser;
import com.mcparser.structures.Question;
import com.mcparser.structures.Test;
import com.mcparser.structures.Topic;

import java.util.ArrayList;
import java.util.List;

public class TopicAnalyser implements ContentAnalyser{

    public List<Topic> extract(Object objectToAnalyze){

        List <String> topicNameList = new ArrayList<String>();
        if(objectToAnalyze instanceof Question){
            topicNameList = analyseQuestion((Question)objectToAnalyze);
        } else if (objectToAnalyze instanceof Test){
            topicNameList = analyseTest((Test) objectToAnalyze);
        } else {
            System.out.println("Object type is not known");
        }

        List <Topic> topicList = new ArrayList<Topic>();
        for(String topicName : topicNameList){
            topicList.add(new Topic(topicName, extractIdFromName(topicName)));
        }

        return topicList;
    }

    /**
     * Analyse the topic of the question and deduce on it.
     *
     * @param question A Question object.
     * @return name of the topic that is decided after analyze
     */
    public List<String> analyseQuestion(Question question) {
        List<String> topicList = new ArrayList<String>();
        topicList.add("Olasilik");
        return topicList;
    }

    /**
     * Analyse the topic of the test and deduce on it.
     *
     * @param test A test object.
     * @return name of the topic that is decided after analyze
     */
    public List<String> analyseTest(Test test) {
        List<String> topicList = new ArrayList<String>();
        topicList.add("Olasilik");
        return topicList;
    }

    /**
     * Extracts the id of the topic from the topic name
     *
     * @param topicName A topic name.
     * @return topic object with Id an Name
     */
    public String extractIdFromName(String topicName){
        String topicId = "-1";
        return topicId;
    }
}
