package com.mcparser.structures;

import com.mcparser.analysers.impl.TopicAnalyser;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Immutable question class.
 * <p>
 * Each question is represented by a question text, 5 answers and a correct
 * response.
 */
public class Question {

    private final String text;
    private final String aChoice;
    private final String bChoice;
    private final String cChoice;
    private final String dChoice;
    private final String eChoice;
    private final CorrectResponse correctResponse;
    private List<Topic> topics;

    public Question(String text, String aChoice, String bChoice, String cChoice,
                    String dChoice, String eChoice, CorrectResponse correctResponse) {

        TopicAnalyser topicAnalyser = new TopicAnalyser();

        // TODO(haliterdogan): Add assertions.
        this.text = text;
        this.aChoice = aChoice;
        this.bChoice = bChoice;
        this.cChoice = cChoice;
        this.dChoice = dChoice;
        this.eChoice = eChoice;
        this.correctResponse = correctResponse;
        this.topics = topicAnalyser.extract(this);
    }

    public String getText() {
        return text;
    }

    public String getaChoice() {
        return aChoice;
    }

    public String getbChoice() {
        return bChoice;
    }

    public String getcChoice() {
        return cChoice;
    }

    public String getdChoice() {
        return dChoice;
    }

    public String geteChoice() {
        return eChoice;
    }

    public CorrectResponse getCorrectResponse() {
        return correctResponse;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public Document createQuestionDocument(String testId){

        Document thisDocument = new Document();

        // create the topic documents of the test
        List<Document> topicList = new ArrayList<Document>();
        for(Topic topic : this.topics){
            topicList.add(new Document("topic_name", topic.getTopicName()).append("topic_id", topic.getTopicId()));
        }

        thisDocument.append("topics"  , topicList);
        thisDocument.append("test"    , this.text);
        thisDocument.append("choiceA" , this.aChoice);
        thisDocument.append("choiceB" , this.bChoice);
        thisDocument.append("choiceC" , this.cChoice);
        thisDocument.append("choiceD" , this.dChoice);
        thisDocument.append("choiceE" , this.eChoice);
        thisDocument.append("testId"  , testId);
        thisDocument.append("correctResponse" , this.correctResponse.toString());
        return thisDocument;
    }

    @Override
    public String toString() {
        String  s  = "Question: " + text   + "\n\n";
                s += "A Choice: " + aChoice + "\n";
                s += "B Choice: " + bChoice + "\n";
                s += "C Choice: " + cChoice + "\n";
                s += "D Choice: " + dChoice + "\n";
                s += "E Choice: " + eChoice + "\n\n";
                s += "Correct Response: " + correctResponse;
        return s;
    }

    // TODO(haliterdogan): toString, hash(), equals(), clone, etc.
}
