package com.mcparser.structures;

public class Subject {

    private final String id;
    private final String subjectName;

    public Subject(String subjectId, String subjectName){
        this.id = subjectId;
        this.subjectName = subjectName;
    }

    public String getSubjectId(){
        return this.id;
    }

    public String getSubjectName(){
        return this.subjectName;
    }
}
