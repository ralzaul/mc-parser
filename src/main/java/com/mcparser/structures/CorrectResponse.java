package com.mcparser.structures;

// Defines the correct response/answer for a question.
public enum CorrectResponse {
    A, B, C, D, E
}
