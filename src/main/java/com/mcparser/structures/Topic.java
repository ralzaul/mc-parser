package com.mcparser.structures;

public class Topic {

    private final String id;
    private final String topicName;

    public Topic(String topicId, String topicName){
        this.id = topicId;
        this.topicName = topicName;
    }

    public String getTopicId(){
        return this.id;
    }

    public String getTopicName(){
        return this.topicName;
    }
}
