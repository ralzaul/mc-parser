package com.mcparser.structures;

import com.mcparser.analysers.impl.SubjectAnalyser;
import com.mcparser.analysers.impl.TopicAnalyser;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines a test (i.e., list of questions).
 */
public class Test {


    private final List<Question> questions;
    private final String name;
    private final List<Image> images;
    private final List<Subject> subjects;
    private final List<Topic> topics;

    public Test(List<Question> questions, List<Image> images, String name) {

        TopicAnalyser topicAnalyser = new TopicAnalyser();
        SubjectAnalyser subjectAnalyser = new SubjectAnalyser();

        this.name = name;
        this.questions = questions;
        this.subjects = subjectAnalyser.extract(this);
        this.topics = topicAnalyser.extract(this);
        this.images = images;
    }

    public Document createTestDocument(String bookId){

        Document thisDocument = new Document();

        // create the subject documents of the test
        List<Document> subjectsList = new ArrayList<Document>();
        for(Subject subject : this.subjects){
            subjectsList.add( new Document("subject_name", subject.getSubjectName()).append("subject_id", subject.getSubjectId()));
        }

        // create the topic documents of the test
        List<Document> topicList = new ArrayList<Document>();
        for(Topic topic : this.topics){
            topicList.add( new Document("topic_name", topic.getTopicName()).append("topic_id", topic.getTopicId()));
        }

        thisDocument.append("count_question" , this.questions.size());
        thisDocument.append("subjects" , subjectsList);
        thisDocument.append("topics", topicList);
        thisDocument.append("book" , bookId);
        return thisDocument;
    }

    @Override
    public String toString() {
        String s = "";
        for (Question question : questions) {
            s += question.toString() + "\n\n\n\n";
            s += "----------------------------\n";
        }
        return s;
    }

    public String getTestName() {return name;}
    public List<Question> getQuestions() {return questions;}
    public List<Subject> getSubjects(){return this.subjects;}
    public List<Topic> getTopics(){return this.topics;}
    public List<Image> getImages(){return this.images;}

    // TODO(haliterdogan): Add clone (copy constructor - deep copy) , hash, equals, toString.
}
