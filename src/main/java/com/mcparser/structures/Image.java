package com.mcparser.structures;

public class Image {

    private final String id;
    private final String imageDirectory;

    public Image(String imageId, String imageDirectory){
        this.id = imageId;
        this.imageDirectory = imageDirectory;
    }

    public String getImageId(){
        return this.id;
    }

    public String getImageDirectory(){
        return this.imageDirectory;
    }

}
