package com.mcparser.clients;

import java.util.List;

import com.mcparser.structures.Question;
import com.mcparser.structures.Test;

/**
 * An interface to the underlying database.
 * <p>
 * It contains all the functions to read/write the DB.
 */
public interface DatabaseClient {

    /**
     * Insert a batch of tests to the DB (atomically).
     *
     * @param tests  List of tests that are going to be inserted.
     * @param bookId Id of the book.
     * @return New Ids of the inserted tests. The indices will match with @tests
     */
    public List<String> insertTests(List<Test> tests, String bookId);
}
