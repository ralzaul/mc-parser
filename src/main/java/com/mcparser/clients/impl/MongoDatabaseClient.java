package com.mcparser.clients.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.mcparser.structures.*;
import com.mcparser.clients.DatabaseClient;
import com.mongodb.DB;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.gridfs.GridFS;
import org.bson.Document;
import org.bson.types.Binary;

import javax.print.Doc;

/**
 * A DbDriver that uses MangoDB.
 */
public class MongoDatabaseClient implements DatabaseClient {

    private final MongoDatabase tebesirDatabase;

    public MongoDatabaseClient(MongoDatabase tebesirDatabase) {
        this.tebesirDatabase = tebesirDatabase;
    }

    public List<String> insertTests(List<Test> tests, String bookId) {

        List<Document> testDocuments = new ArrayList<Document>();
        MongoCollection<Document> testCollection = tebesirDatabase.getCollection("test");

        for (Test currentTest : tests) {

            // create a test document
            Document testDocument = currentTest.createTestDocument(bookId);

            // add the test to the batch
            testDocuments.add(testDocument);
        }

        // insert the tests to the mongodb
        testCollection.insertMany(testDocuments);

        for (int i = 0; i < testDocuments.size(); i++) {

            // insert the questions with inserted test Id's
            this.insertQuestions(tests.get(i).getQuestions(), testDocuments.get(i).get("_id").toString());

            // insert the images with inserted test Id's
            this.insertImages(tests.get(i).getImages(), testDocuments.get(i).get("_id").toString(), bookId);
        }

        return testDocuments.stream().map(document -> document.get("_id").toString()).collect(Collectors.toList());
    }

    /**
     * Insert a batch of images to the DB (atomically).
     * @param images List of images that are going to be inserted.
     * @param testId A test id.
     * @param bookId A book id.
     * @return New Ids of the inserted image. The indices will match with @tests
     */
    private List<String> insertImages(List<Image> images, String testId, String bookId){

        // get the images collection and create indexes on book and test id for fast retrieve
        MongoCollection<Document> imageCollection = tebesirDatabase.getCollection("Image");
        imageCollection.createIndex(new Document("bookId", 1));
        imageCollection.createIndex(new Document("testId", 1));

        List<Document> parsedImages = new ArrayList<>(images.size());

        for(Image image : images){

            String filename = "src/main/" + image.getImageDirectory();
            String name = image.getImageId();

            try{

                // create the file and the image stream
                File imageFile = new File(filename);
                FileInputStream imageInputStream = new FileInputStream(imageFile);
                byte imageByte[] = new byte[imageInputStream.available()];

                // read the byte representation of the image to the input stream
                if(imageInputStream.read(imageByte) != 0){

                    // create the document
                    Binary data = new Binary(imageByte);
                    parsedImages.add(new Document().append("name" , name).append("image" , data).append("testId" , testId).append("bookId" , bookId));
                }

                imageInputStream.close();

            } catch (IOException ioe){
                System.out.println("Failed to read: " + filename);
            }
        }

        // do the insert operation
        imageCollection.insertMany(parsedImages);
        return parsedImages.stream().map(document -> document.get("_id").toString()).collect(Collectors.toList());
    }

    /**
     * Insert a batch of questions to the DB (atomically).
     *
     * @param questions List of questions that are going to be inserted.
     * @param testId Id of the test which the questions belong to
     * @return New Ids of the inserted image. The indices will match with @tests
     */
    private List<String> insertQuestions(List<Question> questions, String testId) {

        List<Document> questionDocuments = new ArrayList<Document>();
        MongoCollection<Document> questionCollection = tebesirDatabase.getCollection("Question");

        for (Question currentQuestion : questions) {

            // create a test document
            Document questionDocument = currentQuestion.createQuestionDocument(testId);

            // add the test to the batch
            questionDocuments.add(questionDocument);
        }

        questionCollection.insertMany(questionDocuments);
        return questionDocuments.stream().map(document -> document.get("_id").toString()).collect(Collectors.toList());
    }
}
