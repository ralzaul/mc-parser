import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.mcparser.structures.Test;
import com.mcparser.clients.DatabaseClient;
import com.mcparser.parsers.AbstractFileParser;
import com.mcparser.replacers.ImageReplacer;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

/**
 * A tool that <li>Parses the given test file (possibly contains more than 1
 * tests) <li>Prepares Test objects (where images are inserted into the DB and
 * question texts are prepared accordingly). <li>Inserts Test objects to the DB.
 * <p>
 * Usage
 * <p>
 * $ Parser <FilePath> <ImageReplacerClassName> <FileReplacerClassName>
 * <DBDriverClassName> <BookId>
 * <p>
 * <FilePath> is the path for the file to be parsed. The class names are the
 * class names to initialize the corresponding objects from. BookId is the DB id
 * of the book that these tests will belong to.
 * <p>
 * Example
 * <p>
 * $ Parser src/main/resources/html-samples/koklu-ifadeler.html
 * com.mcparser.dbdrivers.MangoDbImageReplacer
 * com.mcparser.htmlparsers.EsenIndesignHtmlParser
 * com.mcparser.dbdrivers.MangoDbDriver 12345
 */

public class Parser {

    public static void main(String args[]) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, IOException {

//        if (args.length != 5) {
//            System.err.println("Incorrect arguments. See usage.");
//            return;
//        }

        MongoClientURI uri = new MongoClientURI("mongodb://ralzaul:yesdelft!@ds063779.mongolab.com:63779/tebesir");
        MongoClient client = new MongoClient(uri);
        MongoDatabase mongodb = client.getDatabase("tebesir");

        // TODO(haliterdogan): Use apache command line arguments.
        String filePath = "src/main/resources/html-samples/koklu-ifadeler.html";
        String imageReplacerClassName = "com.mcparser.replacers.impl.MongoImageReplacer";
        String fileParserClassName = "com.mcparser.parsers.impl.EsenHtmlParser";
        String dbDriverClassName = "com.mcparser.clients.impl.MongoDatabaseClient";
        String bookId = "12345";

        File file = new File(filePath);

        System.out.println("---------- INITIALIZING OBJECTS ----------");
        DatabaseClient dbDriver = (DatabaseClient) Class.forName(dbDriverClassName).getConstructor(MongoDatabase.class).newInstance(mongodb);
        ImageReplacer imageReplacer = (ImageReplacer) Class.forName(imageReplacerClassName).getConstructor(DatabaseClient.class).newInstance(dbDriver);
        AbstractFileParser parser = (AbstractFileParser) Class.forName(fileParserClassName).getConstructor(ImageReplacer.class).newInstance(imageReplacer);
        System.out.println("---------- INIT DONE ----------");

        System.out.println("---------- PARSING FILE ----------");
        System.out.println("File: " + file.getAbsolutePath());
        List<Test> tests = parser.parseFile(file);
        System.out.println("---------- PARSING DONE ----------");

        System.out.println("---------- TESTS PARSED ----------");
        tests.stream().forEach(s -> System.out.println(s.toString()));
        System.out.println("---------- TESTS PARSED ----------");

        System.out.println("---------- INSERTING TESTS TO DB --------");
//        List<String> testIds = dbDriver.insertTests(tests, bookId);
//        System.out.println(testIds.size() + " tests are inserted to the DB with the following Ids.");
        System.out.println("---------- INSERT DONE ----------");
    }
}
